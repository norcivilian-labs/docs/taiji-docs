# Design

single page application with tools for practicing qigon and taiji 

competes: ??

interacts: users on their phones

constitutes: single page web application

includes: store with data, several screens

resembles: https://www.wondrous-badi.today/

patterns: reactive ui, redux

stakeholders: fetsorn, heorgt

mobile first

# stack
React

vite

# development environment
macos development

vscode 

git

nvm

# user path
## happy path
 - open app
 - choose exercise from the table 
 - view exercise
 - repeat exercise for 15 min with interactive counter
 - see history progress 
 - make journal notes
 - choose different exercise
 - repeat exercise for 15 min
 - see history progress
 - make journal notes
 - close app
## only contacts
 - open app
 - view contact "for personal help"
 - contact heorg
 - suscribe in tg
## only notes
 - open app
 - view exercise
   - make notes
   - see history of notes
   - sync with calendar
   - see calendar diary
 - exit
 - view exercise
 - exit
 - view exercise
 - close app
# screens
 - home
   - choose exercise from table
   - choose exercise from favorite list 
   - view history (each exercise / all)
   - read journal
   - contact heorg
 - exercise page
 - history chart
 - journal
 - contact
