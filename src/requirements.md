# Requirements

 - user must see GIF of 24 forms
 - user must read instructions for every form
 - user must see contacts of heorgt, email
 
 - user must choose sit/stand/lie
 - user must choose axes/spirals/circles
 - user must choose hands/ban/mace
 - user must see pages with description of form - GIF, photos, video, text instructions
 
 - user must see history of watches
 - user can see favourite exercies
 
 - user must see a chart of practice history with accrued hours of practice
 
 - user must NOT register

 - user can see interactive counter with a pleasant sound for every repetitin of the form

 - user can see a sequence of exercises with recommendation for exercise
 - user can pass a test to get a test

 - user MUST NOT communicate with others

 - developer must add new axes to table

